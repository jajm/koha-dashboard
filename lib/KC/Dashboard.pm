package KC::Dashboard;

# Script to create dashboard.koha-community.org
# Copyright chris@bigballofwax.co.nz 2012,2013

our $VERSION = '0.1';

use Dancer;
use Dancer::Plugin::Database;
use DateTime;

use Modern::Perl;
use KC::Data ':all';
use DBI;
use Template;
use Text::CSV;

my $server_url = 'https://dashboard.koha-community.org';

set 'session' => 'Simple';

set 'show_errors'  => 1;
set 'startup_info' => 1;
set 'warnings'     => 1;

get '/' => sub {
    my $bugs_dbh = database('bugs');

    if ($DEBUG) {
        my $max_ts = $bugs_dbh->selectrow_array(
            q|SELECT DATE(MAX(creation_ts)) FROM bugs|);
        ( $cur_year, $cur_month, $cur_day ) = split( '-', $max_ts );
        $now = DateTime->new(
            year  => $cur_year,
            month => $cur_month,
            day   => $cur_day
        );
        $server_url = 'http://localhost:3000';
    }
    else {
        $now = DateTime->now;
    }

    set_now($now);

    my $entries    = last5signoffs($bugs_dbh);
    my $stats      = monthlyactivity( $bugs_dbh, 'Signed Off' );
    my $qa         = monthlyactivity( $bugs_dbh, 'Passed QA' );
    my $failedqa   = monthlyactivity( $bugs_dbh, 'Failed QA' );
    my $yearsign   = yearlyactivity( $bugs_dbh, 'Signed Off' );
    my $yearpass   = yearlyactivity( $bugs_dbh, 'Passed QA' );
    my $yearfail   = yearlyactivity( $bugs_dbh, 'Failed QA' );
    my $rescued    = monthlyrescues($bugs_dbh);
    my $yearresc   = yearlyrescues($bugs_dbh);
    my $documented = monthlydocs($bugs_dbh);
    my $yeardocs   = yearlydocs($bugs_dbh);

    # enhancements & new features
    my $sql = q{
        SELECT b.bug_id, b.short_desc, MAX(ba.bug_when) AS bug_when, b.bug_severity, co.name AS component_name, IF(po.realname = 'Bugs List', '', po.realname) AS assignee
        FROM bugs b
            JOIN bugs_activity ba ON b.bug_id = ba.bug_id
            JOIN components co ON b.component_id = co.id
            JOIN profiles po ON b.assigned_to = po.userid
        WHERE b.product_id = 2
            AND b.component_id <> 44
            AND (ba.added = 'Pushed to Main' OR ba.added = 'Needs documenting')
            AND (b.bug_severity = 'enhancement' OR b.bug_severity = 'new feature')
        GROUP BY b.bug_id
        ORDER BY bug_when DESC
        LIMIT 10
    };
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute                      or die $sth->errstr;
    my $enhancement = $sth->fetchall_arrayref({});
    my $dates       = get_dates();

    # needs signoff
    $sql = q{
        SELECT b.bug_id, b.short_desc, MAX(bug_when) AS bug_when, bug_severity, co.name AS component_name, IF(po.realname = 'Bugs List', '', po.realname) AS assignee
        FROM bugs b
            JOIN bugs_activity ba ON b.bug_id = ba.bug_id
            JOIN components co ON b.component_id = co.id
            JOIN profiles po ON b.assigned_to = po.userid
        WHERE b.product_id = 2
            AND b.bug_status = 'Needs Signoff'
            AND ba.added = 'Needs Signoff'
        GROUP BY b.bug_id
        ORDER BY bug_when DESC
        LIMIT 25
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute                   or die $sth->errstr;
    my $needs_signoff = $sth->fetchall_arrayref({});

    # needs qa
    $sql = q{
        SELECT b.bug_id, short_desc, MAX(bug_when) AS bug_when, bug_severity, co.name AS component_name, IF(po.realname = 'Bugs List', '', po.realname) AS assignee, IF(qa.realname = 'Testopia', '', qa.realname) AS qa_contact
        FROM bugs b
            JOIN bugs_activity ba ON b.bug_id = ba.bug_id
            JOIN components co ON b.component_id = co.id
            JOIN profiles po ON b.assigned_to = po.userid
            JOIN profiles qa ON b.qa_contact = qa.userid
        WHERE b.product_id = 2
            AND b.bug_status = 'Signed Off'
            AND ba.added = 'Signed Off'
        GROUP BY b.bug_id
        ORDER BY FIELD(bug_severity, 'blocker', 'critical', 'major', 'normal', 'minor', 'trivial', 'enhancement', 'new feature') ASC, bug_when DESC
        LIMIT 25
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute                   or die $sth->errstr;
    my $needs_qa = $sth->fetchall_arrayref({});

    # needs documenting
    $sql = q{
        SELECT b.bug_id, short_desc, lastdiffed AS bug_when, bug_severity, co.name AS component_name, IF(po.realname = 'Bugs List', '', po.realname) AS assignee
        FROM bugs b
            JOIN components co ON b.component_id = co.id
            JOIN profiles po ON b.assigned_to = po.userid
        WHERE b.product_id = 2
            AND (bug_status = 'Needs documenting' OR (component_id = 44 AND bug_status NOT IN ('ASSIGNED', 'RESOLVED', 'CLOSED')))
        ORDER BY component_id = 44 DESC, lastdiffed
        LIMIT 50
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute                   or die $sth->errstr;
    my $needs_documenting = $sth->fetchall_arrayref({});

    # needs push
    $sql = q{
        SELECT b.bug_id, short_desc, MIN(bug_when) AS bug_when, bug_severity, co.name AS component_name, IF(po.realname = 'Bugs List', '', po.realname) AS assignee, IF(qa.realname = 'Testopia', '', qa.realname) AS qa_contact
        FROM bugs b
            JOIN bugs_activity ba ON b.bug_id = ba.bug_id
            JOIN components co ON b.component_id = co.id
            JOIN profiles po ON b.assigned_to = po.userid
            JOIN profiles qa ON b.qa_contact = qa.userid
        WHERE b.product_id = 2
            AND b.bug_status = 'Passed QA'
            AND ba.added = 'Passed QA'
        GROUP BY b.bug_id
        ORDER BY FIELD(bug_severity, 'trivial', 'minor', 'normal', 'major', 'critical', 'blocker') DESC, bug_when
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute                   or die $sth->errstr;
    my $needs_push = $sth->fetchall_arrayref({});

    my $packages = get_package_versions();

    # queue stats
    $sql = "SELECT count(*) as count,bug_status FROM bugs WHERE product_id = 2 GROUP BY bug_status";
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    $sql =
"SELECT count(*) as count,bug_status FROM bugs WHERE product_id = 2 AND bug_severity <> 'enhancement'
    AND bug_severity <> 'new feature' GROUP BY bug_status";
    my $sth2 = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth2->execute;
    my $all      = $sth->fetchall_hashref('bug_status');
    my $bugs     = $sth2->fetchall_hashref('bug_status');
    my @statuses = (
        "Needs documenting",
        "Needs Signoff",
        "Signed Off",
        "Passed QA",
        "Pushed to main",
        "Failed QA",
        "Patch doesn't apply",
        "In Discussion",
    );
    my ( $status, $bugssign );

    for my $s (@statuses) {
        $status->{$s}   = $all->{$s}{count}  || 0;
        $bugssign->{$s} = $bugs->{$s}{count} || 0;
    }

    $sql = "SELECT count(*) AS count FROM keywords WHERE keywordid = 19";
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    my $needs_additional_work = $sth->fetchrow_hashref;
    $status->{"Needs additional work"} = $needs_additional_work->{count} || 0;

    template 'show_entries.tt', {
        'server_url'        => $server_url,
        'entries'           => $entries,
        'stats'             => $stats,
        'enhancments'       => $enhancement,
        'dates'             => $dates,
        'qa'                => $qa,
        'failed'            => $failedqa,
        'needs_signoff'     => $needs_signoff,
        'needs_documenting' => $needs_documenting,
        'needs_qa'          => $needs_qa,
        'needs_push'        => $needs_push,
        'rescued'           => $rescued,
        'documented'        => $documented,
        'yearsign'          => $yearsign,
        'yearpass'          => $yearpass,
        'yearfail'          => $yearfail,
        'yearresc'          => $yearresc,
        'yeardocs'          => $yeardocs,
        'year'              => $cur_year,
        'month_name'        => DateTime->now->month_name,
        (
            $DEBUG
            ? (
                debug_mode => $DEBUG,
                cur_year   => $cur_year,
                cur_month  => $cur_month,
                cur_day    => $cur_day,
              )
            : ()
        ),

        #        'devs'        => $devs,
        #        'ohloh'       => $ohloh,
        packages           => $packages,
        supported_versions => get_supported_versions(),
        status             => $status,
        bugssign           => $bugssign,
    };
};

get '/my_bugs/:user' => sub {
    my $user     = param('user');
    my $bugs_dbh = database('bugs');

    if ($DEBUG) {
        my $max_ts = $bugs_dbh->selectrow_array(
            q|SELECT DATE(MAX(creation_ts)) FROM bugs|);
        ( $cur_year, $cur_month, $cur_day ) = split( '-', $max_ts );
        $now = DateTime->new(
            year  => $cur_year,
            month => $cur_month,
            day   => $cur_day
        );
        $server_url = 'http://localhost:3000';
    }
    else {
        $now = DateTime->now;
    }

    set_now($now);

    my $stats_month = userstats( $bugs_dbh, $user, 'month' );
    my $stats_year  = userstats( $bugs_dbh, $user, 'year' );
    my $stats       = userstats( $bugs_dbh, $user );

    my $sql =
"SELECT IF(realname = '', 'Anonymous contributor', realname) FROM profiles WHERE profiles.userid = ?";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($user)               or die $sth->errstr;
    my ($realname) = $sth->fetchrow_array;

    # reported
    $sql = q{
        SELECT b.bug_id, b.short_desc, b.creation_ts AS bug_when, b.bug_severity, co.name AS component_name, b.bug_status, IF(po.realname = 'Bugs List', '', po.realname) AS assignee
        FROM bugs b
            JOIN components co ON b.component_id = co.id
            JOIN profiles po ON b.assigned_to = po.userid
        WHERE b.product_id = 2
            AND b.reporter = ?
            AND b.assigned_to != ?
            AND bug_status NOT IN ('CLOSED', 'RESOLVED')
        GROUP BY b.bug_id
        ORDER BY
            FIELD(bug_status, 'NEW', 'UNCONFIRMED', 'VERIFIED', 'ASSIGNED', 'BLOCKED', 'REOPENED', 'In Discussion', 'Patch doesn\'t apply', 'Failed QA', 'Needs Signoff', 'Signed Off', 'Passed QA', 'Pushed to main', 'Pushed to oldoldoldstable', 'Pushed to oldoldstable', 'Pushed to oldstable', 'Pushed to stable', 'Needs documenting') ASC,
            creation_ts DESC
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute( $user, $user )   or die $sth->errstr;
    my $reported;
    my $status = '';
    my $i      = -1;

    for my $row ( @{ $sth->fetchall_arrayref({}) } ) {
        if ( $row->{bug_status} eq $status ) {
            push @{ $reported->[$i] }, $row;
        }
        else {
            $i++;
            $status = $row->{bug_status};
            push @{ $reported->[$i] }, $row;
        }
    }

    # needs additional work
    $sql = q{
        SELECT b.bug_id, short_desc, MAX(bug_when) AS bug_when, bug_severity, co.name AS component_name, bug_status
        FROM bugs b
            JOIN bugs_activity ba ON b.bug_id = ba.bug_id
            JOIN keywords kw ON b.bug_id = kw.bug_id
            JOIN components co ON b.component_id = co.id
        WHERE b.product_id = 2
            AND kw.keywordid = 19
            AND ba.added = 'additional_work_needed'
            AND b.assigned_to = ?
        GROUP BY b.bug_id
        ORDER BY FIELD(bug_severity, 'blocker', 'critical', 'major', 'normal', 'minor', 'trivial', 'enhancement', 'new feature') ASC, bug_when DESC
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($user)            or die $sth->errstr;
    my $needs_additional_work = $sth->fetchall_arrayref({});

    # needs_work
    $sql = q{
        SELECT bug_id, short_desc, lastdiffed AS bug_when, bug_severity, co.name AS component_name, bug_status, IF(qa.realname = 'Testopia', '', qa.realname) AS qa_contact
        FROM bugs b
            JOIN components co ON b.component_id = co.id
            JOIN profiles qa ON b.qa_contact = qa.userid
        WHERE b.product_id = 2
            AND b.assigned_to = ?
            AND b.bug_status IN ('Patch doesn\'t apply', 'Failed QA', 'In Discussion', 'ASSIGNED', 'NEW')
        GROUP BY b.bug_id
        ORDER BY FIELD(bug_status, 'Patch doesn\'t apply', 'Failed QA', 'In Discussion', 'ASSIGNED', 'NEW') ASC, lastdiffed DESC
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($user)            or die $sth->errstr;
    my $needs_work;
    $status = '';
    $i      = -1;

    for my $row ( @{ $sth->fetchall_arrayref({}) } ) {
        if ( $row->{bug_status} eq $status ) {
            push @{ $needs_work->[$i] }, $row;
        }
        else {
            $i++;
            $status = $row->{bug_status};
            push @{ $needs_work->[$i] }, $row;
        }
    }

    # waiting
    $sql = q{
        SELECT bug_id, short_desc, lastdiffed AS bug_when, bug_severity, co.name AS component_name, CASE 
            WHEN bug_status = 'Signed Off' THEN 'Needs QA'
            WHEN bug_status = 'Passed QA' THEN 'Ready to push'
            ELSE bug_status
        END AS bug_status
        FROM bugs b
            JOIN components co ON b.component_id = co.id
        WHERE b.product_id = 2
            AND b.assigned_to = ?
            AND b.bug_status IN ('Needs Signoff', 'Signed Off', 'Passed QA')
        GROUP BY 
            bug_id
        ORDER BY 
            FIELD(bug_status, 'Needs Signoff', 'Signed Off', 'Passed QA') ASC, lastdiffed DESC
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($user)            or die $sth->errstr;
    my $waiting;
    $status = '';
    $i      = -1;

    for my $row ( @{ $sth->fetchall_arrayref({}) } ) {
        if ( $row->{bug_status} eq $status ) {
            push @{ $waiting->[$i] }, $row;
        }
        else {
            $i++;
            $status = $row->{bug_status};
            push @{ $waiting->[$i] }, $row;
        }
    }

    # claimed qa
    $sql = q{
        SELECT bug_id, short_desc, lastdiffed AS bug_when, bug_severity, co.name AS component_name, bug_status
        FROM bugs b
            JOIN components co ON b.component_id = co.id
        WHERE b.product_id = 2
            AND b.qa_contact = ?
            AND b.bug_status IN ('NEW', 'ASSIGNED', 'In Discussion', 'Failed QA', 'Patch doesn\'t apply', 'Needs Signoff', 'Signed Off', 'Passed QA')
        ORDER BY 
            FIELD(bug_status, 'Signed Off', 'Needs Signoff', 'Failed QA', 'Passed QA', 'NEW', 'ASSIGNED', 'In Discussion', 'Patch doesn\'t apply'),
            FIELD(bug_severity, 'blocker', 'critical', 'major', 'normal', 'minor', 'trivial', 'enhancement', 'new feature') ASC,
            lastdiffed DESC
    };
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute($user)            or die $sth->errstr;
    my $claimed_qa;
    $status = '';
    $i      = -1;

    for my $row ( @{ $sth->fetchall_arrayref({}) } ) {
        if ( $row->{bug_status} eq $status ) {
            push @{ $claimed_qa->[$i] }, $row;
        }
        else {
            $i++;
            $status = $row->{bug_status};
            push @{ $claimed_qa->[$i] }, $row;
        }
    }

    template 'my_bugs.tt',
      {
        realname              => $realname,
        stats_month           => $stats_month,
        stats_year            => $stats_year,
        stats                 => $stats,
        reported              => $reported,
        needs_additional_work => $needs_additional_work,
        needs_work            => $needs_work,
        waiting               => $waiting,
        claimed_qa            => $claimed_qa
      };
};

get '/find_my_bugs' => sub {
    my $name     = param('name');
    my $bugs_dbh = database('bugs');
    $name .= '%';

    my $sql =
"SELECT realname, userid FROM profiles WHERE realname LIKE ? OR userid = ?";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute( $name, $name )      or die $sth->errstr;
    my $results = $sth->fetchall_arrayref();

    if ( scalar(@{$results}) == 1 ) {
        return redirect "/my_bugs/" . $results->[0][1];
    }
    else {
        template 'find_my_bugs.tt',
          { users => $results };
    }
};

get '/bug_status' => sub {
    my $bugs_dbh = database('bugs');
    my $sql =
      "SELECT count(*) as count,bug_status FROM bugs GROUP BY bug_status";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute                      or die $sth->errstr;
    $sql =
"SELECT count(*) as count,bug_status FROM bugs WHERE bug_severity <> 'enhancement'
    AND bug_severity <> 'new feature' GROUP BY bug_status";
    my $sth2 = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth2->execute;
    my $all      = $sth->fetchall_hashref('bug_status');
    my $bugs     = $sth2->fetchall_hashref('bug_status');
    my @statuses = (
        "Needs documenting",
        "Needs Signoff",
        "Signed Off",
        "Passed QA",
        "Pushed to main",
        "Failed QA",
        "Patch doesn't apply",
        "In Discussion",
    );
    my ( $status, $bugssign );

    for my $s (@statuses) {
        $status->{$s}   = $all->{$s}{count}  || 0;
        $bugssign->{$s} = $bugs->{$s}{count} || 0;
    }

    $sql = "SELECT count(*) AS count FROM keywords WHERE keywordid = 19";
    $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    $sth->execute or die $sth->errstr;
    my $needs_additional_work = $sth->fetchrow_hashref;

    $status->{"Needs additional work"} = $needs_additional_work->{count} || 0;

    template 'bug_status.tt',
      {
        'status'   => $status,
        'bugssign' => $bugssign,
      };
};

get '/bz_status' => sub {
    my $hide_details  = param 'hide_details';
    my $bugs_dbh      = database('bugs');
    my $health_status = health_status($bugs_dbh);
    template 'bz_status.tt',
      { health_status => $health_status, hide_details => $hide_details, };
};

get '/pushed_by_day' => sub {
    my $bugs_dbh = database('bugs');
    my $pushedD  = pushed_by_day($bugs_dbh);
    content_type 'application/json';
    return to_json $pushedD;
};

get '/activity_by_day' => sub {
    my $bugs_dbh = database('bugs');

    if ($DEBUG) {
        my $max_ts = $bugs_dbh->selectrow_array(
            q|SELECT DATE(MAX(creation_ts)) FROM bugs|);
        ( $cur_year, $cur_month, $cur_day ) = split( '-', $max_ts );
        $now = DateTime->new(
            year  => $cur_year,
            month => $cur_month,
            day   => $cur_day
        );
        $server_url = 'http://localhost:3000';
    }
    else {
        $now = DateTime->now;
    }

    my $activity = [];

    my $sql =
"SELECT count(*) as count, DATE(bug_when) as day FROM bugs_activity WHERE date(bug_when) = ? GROUP BY DATE(bug_when);";
    my $sth = $bugs_dbh->prepare($sql) or die $bugs_dbh->errstr;
    for ( my $i = 0 ; $i < 7 ; $i++ ) {
        my $date = $now->clone->subtract( days => $i );
        $sth->execute( $date->ymd('/') ) or die $sth->errstr;
        my $day = $sth->fetchrow_hashref();
        if ($day) {
            push @{$activity},
              { x => $day->{day} . "T00:00:00Z", y => $day->{count} };
        }
        else {
            push @{$activity}, { x => "$date", y => 0 };
        }
    }
    content_type 'application/json';
    return to_json {
        datasets => [ { data => $activity, label => "Activity" } ] };
};

get '/randombug' => sub {
    my $sql =
"SELECT * FROM (SELECT bug_id,short_desc FROM bugs WHERE bug_status NOT in 
    ('CLOSED','RESOLVED','Pushed to main','Pushed to stable','Pushed to oldstable','Pushed to oldoldstable','VERIFIED', 'Signed Off', 'Passed QA') ) AS bugs2 ORDER BY rand() LIMIT 1";
    my $sth = database('bugs')->prepare($sql) or die database('bugs')->errstr;
    $sth->execute                             or die $sth->errstr;

    template 'randombug.tt', { 'randombug' => $sth->fetchall_arrayref({}) };
};

get '/randomquote' => sub {
    open FILE, 'data/koha_irc_quotes.txt' || die "can't open file";
    my @quotes    = <FILE>;
    my $quote     = $quotes[ rand @quotes ];
    my $csv       = Text::CSV->new( { binary => 1 } );
    my $linequote = $csv->parse($quote);
    template 'quote.tt', { 'quote' => $csv };
};

get '/rq' => sub {
    open FILE, 'data/koha_irc_quotes.txt' || die "can't open file";
    my @quotes    = <FILE>;
    my $quote     = $quotes[ rand @quotes ];
    my $csv       = Text::CSV->new( { binary => 1 } );
    my $linequote = $csv->parse($quote);
    template 'quotetext.tt', { 'quote' => $csv };
};

true;
