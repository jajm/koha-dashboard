<%- BLOCK bugview -%>
  <li class="status-<%- entry.bug_severity | replace('\s', '-') | uri -%>">
    <a class="link-success" href="https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=<% entry.bug_id | uri %>"><% entry.bug_id | html %></a><% IF entry.qa_contact %> + <% ELSE %> - <% END %><span data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="<ul><li>Date: <% entry.bug_when | html %></li><%- IF entry.assignee -%><li>Assignee: <% entry.assignee | html_entity %></li><%- END -%><%- IF entry.qa_contact -%><li>QA Contact: <% entry.qa_contact | html_entity %></li><%- END -%><li>Type: <% entry.bug_severity | html %></li><li>Component: <% entry.component_name | html %></li></ul>"><% entry.short_desc | html %></span>
  </li>
<%- END -%>
